﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertTool
{
	public partial class FileOpenDialog : Form
	{
		public FileOpenDialog()
		{
			InitializeComponent();
		}

		public string FileName
		{
			get { return fileOpenPart.FileName; }
		}

		private void FileOpenDialog_Load(object sender, EventArgs e)
		{
			UpdateActions();
		}

		private void UpdateActions()
		{
			buttonOk.Enabled = fileOpenPart.FileName.Length > 0;
		}

		private void fileOpenPart_FileNameChanged(object sender, EventArgs e)
		{
			UpdateActions();
		}
	}
}
