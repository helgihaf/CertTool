﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Specialized;

namespace CertTool
{
	public partial class StoreView : UserControl
	{
		public StoreView()
		{
			InitializeComponent();
			comboBoxStoreLocation.DataSource = Enum.GetValues(typeof(StoreLocation));
			comboBoxStoreName.DataSource = Enum.GetValues(typeof(StoreName));
			comboBoxFindType.DataSource = Enum.GetValues(typeof(X509FindType));
		}

		public StoreSearch StoreSearch
		{
			get { return StoreSearchFromView(); }
			set
			{
				if (value != null)
				{
					StoreSearchToView(value);
				}
			}
		}

		public IList<string> FindValueHistory
		{
			get { return FindValueHistoryFromView(); }
			set
			{
				if (value != null)
				{
					FindValueHistoryToView(value);
				}
			}
		}

		public IList<ViewModel.ItemCertificate> SelectedItems
		{
			get
			{
				return certListPart.SelectedItems;
			}
		}

		public event EventHandler<EventArgs> SelectedCertificatesChanged
		{
			add
			{
				certListPart.SelectedCertificatesChanged += value;
			}
			remove
			{
				certListPart.SelectedCertificatesChanged -= value;
			}
		}

		public event EventHandler<EventArgs> SelectionActivated
		{
			add
			{
				certListPart.SelectionActivated += value;
			}
			remove
			{
				certListPart.SelectionActivated -= value;
			}
		}

		private void buttonSearch_Click(object sender, EventArgs e)
		{
			Search();
		}

		private void Search()
		{
			var storeSearch = StoreSearchFromView();
			AddValueToHistory(storeSearch.FindValue);
			Search(storeSearch);
		}

		private void AddValueToHistory(string findValue)
		{
			AddValueToComboBoxHistory(findValue, comboBoxValue.Items);
		}

		private void AddValueToComboBoxHistory(string findValue, ComboBox.ObjectCollection items)
		{
			if (string.IsNullOrEmpty(findValue))
			{
				return;
			}

			int index = items.IndexOf(findValue);
			if (index == 0)
			{
				return;
			}
			else if (index > 0)
			{
				items.RemoveAt(index);
			}

			while (comboBoxValue.Items.Count >= 16)
			{
				comboBoxValue.Items.RemoveAt(comboBoxValue.Items.Count - 1);
			}

			comboBoxValue.Items.Insert(0, findValue);
		}

		private StoreSearch StoreSearchFromView()
		{
			return new StoreSearch
			{
				Location = (StoreLocation)comboBoxStoreLocation.SelectedValue,
				StoreName = (StoreName)comboBoxStoreName.SelectedValue,
				FindType = (X509FindType)comboBoxFindType.SelectedValue,
				FindValue = comboBoxValue.Text.Trim(),
			};
		}

		private void StoreSearchToView(StoreSearch storeSearch)
		{
			comboBoxStoreLocation.SelectedItem = storeSearch.Location;
			comboBoxStoreName.SelectedItem = storeSearch.StoreName;
			comboBoxFindType.SelectedItem = storeSearch.FindType;
			comboBoxValue.Text = storeSearch.FindValue;
		}


		private IList<string> FindValueHistoryFromView()
		{
			return comboBoxValue.Items.OfType<string>().ToList();
		}

		private void FindValueHistoryToView(IList<string> items)
		{
			comboBoxValue.Items.Clear();
			comboBoxValue.Items.AddRange(items.ToArray());
		}


		private async void Search(StoreSearch search)
		{
			Cursor.Current = Cursors.AppStarting;
			certListPart.ClearData();
			var certificates = await GetCertificatesAsync(search);
			certListPart.AddData(certificates.Select(
				c => new ViewModel.ItemCertificate
				{
					Certificate = c,
					Location = new ViewModel.X509StoreLocation { Location = search.Location, StoreName = search.StoreName }
				}));
		}

		private Task<IList<X509Certificate2>> GetCertificatesAsync(StoreSearch search)
		{
			return Task.Factory.StartNew(() => GetCertificates(search));
		}

		private IList<X509Certificate2> GetCertificates(StoreSearch search)
		{
			var list = new List<X509Certificate2>();
			using (X509Store store = new X509Store(search.StoreName, search.Location))
			{
				store.Open(OpenFlags.ReadOnly);
				X509Certificate2Collection foundCerts;
				if (!string.IsNullOrEmpty(search.FindValue))
				{
					foundCerts = store.Certificates.Find(search.FindType, search.FindValue, false);
				}
				else
				{
					foundCerts = store.Certificates;
				}

				foreach (var cert in foundCerts)
				{
					list.Add(cert);
				}
				return list;
			}
		}

	}
}
