﻿namespace CertTool
{
	partial class FileView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fileOpenPart1 = new CertTool.FileOpenPart();
			this.SuspendLayout();
			// 
			// fileOpenPart1
			// 
			this.fileOpenPart1.FileName = "";
			this.fileOpenPart1.Location = new System.Drawing.Point(15, 14);
			this.fileOpenPart1.Name = "fileOpenPart1";
			this.fileOpenPart1.Size = new System.Drawing.Size(585, 38);
			this.fileOpenPart1.TabIndex = 0;
			// 
			// FileView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.fileOpenPart1);
			this.Name = "FileView";
			this.Size = new System.Drawing.Size(615, 362);
			this.ResumeLayout(false);

		}

		#endregion

		private FileOpenPart fileOpenPart1;
	}
}
