﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertTool
{
	[SettingsSerializeAs(SettingsSerializeAs.Xml)]
	public class StoreSearch
	{
		public StoreLocation Location { get; set; }
		public StoreName StoreName { get; set; }
		public X509FindType FindType { get; set; }
		public string FindValue { get; set; }
	}
}
