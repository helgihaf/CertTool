﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertTool
{
	public partial class FileOpenPart : UserControl
	{
		public FileOpenPart()
		{
			InitializeComponent();
		}

		public string FileName
		{
			get { return textBoxFile.Text; }
			set { textBoxFile.Text = value; }
		}

		public event EventHandler<EventArgs> FileNameChanged;

		private void buttonBrowse_Click(object sender, EventArgs e)
		{
			openFileDialog.FileName = textBoxFile.Text;
			if (openFileDialog.ShowDialog(this) == DialogResult.OK)
			{
				textBoxFile.Text = openFileDialog.FileName;
			}
		}

		private void textBoxFile_TextChanged(object sender, EventArgs e)
		{
			FileNameChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}
