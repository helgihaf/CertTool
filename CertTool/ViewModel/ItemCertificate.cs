﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertTool.ViewModel
{
	public class ItemCertificate
	{
		public X509Certificate2 Certificate { get; set; }
		public CertificateLocation Location { get; set; }
	}
}
