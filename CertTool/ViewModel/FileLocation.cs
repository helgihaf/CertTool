﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertTool.ViewModel
{
	public class FileLocation : CertificateLocation
	{
		public string FullPath { get; set; }
	}
}
