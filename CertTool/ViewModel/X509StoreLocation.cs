﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertTool.ViewModel
{
	public class X509StoreLocation : CertificateLocation
	{
		public StoreLocation Location { get; set; }
		public StoreName StoreName { get; set; }
	}
}
