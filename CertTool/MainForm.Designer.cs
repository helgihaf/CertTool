﻿namespace CertTool
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openCertificateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFromStoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.splitContainer = new System.Windows.Forms.SplitContainer();
			this.certListPart = new CertTool.CertListPart();
			this.certView = new CertTool.CertView2();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
			this.splitContainer.Panel1.SuspendLayout();
			this.splitContainer.Panel2.SuspendLayout();
			this.splitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openCertificateToolStripMenuItem,
            this.openFromStoreToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// openCertificateToolStripMenuItem
			// 
			this.openCertificateToolStripMenuItem.Name = "openCertificateToolStripMenuItem";
			this.openCertificateToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.openCertificateToolStripMenuItem.Text = "Open from &File...";
			this.openCertificateToolStripMenuItem.Click += new System.EventHandler(this.openCertificateToolStripMenuItem_Click);
			// 
			// openFromStoreToolStripMenuItem
			// 
			this.openFromStoreToolStripMenuItem.Name = "openFromStoreToolStripMenuItem";
			this.openFromStoreToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.openFromStoreToolStripMenuItem.Text = "Open from &Store...";
			this.openFromStoreToolStripMenuItem.Click += new System.EventHandler(this.openFromStoreToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			// 
			// splitContainer
			// 
			this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer.Location = new System.Drawing.Point(0, 24);
			this.splitContainer.Name = "splitContainer";
			// 
			// splitContainer.Panel1
			// 
			this.splitContainer.Panel1.Controls.Add(this.certListPart);
			// 
			// splitContainer.Panel2
			// 
			this.splitContainer.Panel2.Controls.Add(this.certView);
			this.splitContainer.Size = new System.Drawing.Size(1008, 705);
			this.splitContainer.SplitterDistance = 336;
			this.splitContainer.TabIndex = 1;
			// 
			// certListPart
			// 
			this.certListPart.AllowFileDrop = true;
			this.certListPart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.certListPart.Location = new System.Drawing.Point(0, 0);
			this.certListPart.MultiSelect = false;
			this.certListPart.Name = "certListPart";
			this.certListPart.SelectedItem = null;
			this.certListPart.Size = new System.Drawing.Size(336, 705);
			this.certListPart.TabIndex = 0;
			this.certListPart.SelectedCertificatesChanged += new System.EventHandler<System.EventArgs>(this.certListPart_SelectedCertificatesChanged);
			this.certListPart.FileOpenRequested += new System.EventHandler<CertTool.FileOpenRequestEventArgs>(this.certListPart_FileOpenRequested);
			// 
			// certView
			// 
			this.certView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.certView.Location = new System.Drawing.Point(0, 0);
			this.certView.Name = "certView";
			this.certView.Size = new System.Drawing.Size(668, 705);
			this.certView.TabIndex = 0;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1008, 729);
			this.Controls.Add(this.splitContainer);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "CertTool";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer.Panel1.ResumeLayout(false);
			this.splitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
			this.splitContainer.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openCertificateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openFromStoreToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SplitContainer splitContainer;
		private CertListPart certListPart;
		private CertView2 certView;
	}
}

