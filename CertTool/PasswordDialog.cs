﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertTool
{
	public partial class PasswordDialog : Form
	{
		public PasswordDialog()
		{
			InitializeComponent();
		}

		public string FilePath
		{
			get { return labelFilePath.Text; }
			set { labelFilePath.Text = value; }
		}

		public string Password
		{
			get { return textBoxPassword.Text; }
		}

		private void PasswordDialog_Load(object sender, EventArgs e)
		{
			textBoxPassword.Clear();
			textBoxPassword.Focus();
			UpdateActions();
		}

		private void textBoxPassword_TextChanged(object sender, EventArgs e)
		{
			UpdateActions();
		}

		private void UpdateActions()
		{
			buttonOk.Enabled = textBoxPassword.TextLength > 0;
		}
	}
}
