﻿namespace CertTool
{
	partial class StoreView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxStoreLocation = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxStoreName = new System.Windows.Forms.ComboBox();
			this.comboBoxFindType = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.comboBoxValue = new System.Windows.Forms.ComboBox();
			this.buttonSearch = new System.Windows.Forms.Button();
			this.certListPart = new CertTool.CertListPart();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Store Location";
			// 
			// comboBoxStoreLocation
			// 
			this.comboBoxStoreLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxStoreLocation.FormattingEnabled = true;
			this.comboBoxStoreLocation.Location = new System.Drawing.Point(0, 16);
			this.comboBoxStoreLocation.Name = "comboBoxStoreLocation";
			this.comboBoxStoreLocation.Size = new System.Drawing.Size(193, 21);
			this.comboBoxStoreLocation.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(196, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Store Name";
			// 
			// comboBoxStoreName
			// 
			this.comboBoxStoreName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxStoreName.FormattingEnabled = true;
			this.comboBoxStoreName.Location = new System.Drawing.Point(199, 16);
			this.comboBoxStoreName.Name = "comboBoxStoreName";
			this.comboBoxStoreName.Size = new System.Drawing.Size(193, 21);
			this.comboBoxStoreName.TabIndex = 3;
			// 
			// comboBoxFindType
			// 
			this.comboBoxFindType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxFindType.FormattingEnabled = true;
			this.comboBoxFindType.Location = new System.Drawing.Point(398, 16);
			this.comboBoxFindType.Name = "comboBoxFindType";
			this.comboBoxFindType.Size = new System.Drawing.Size(201, 21);
			this.comboBoxFindType.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(395, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(54, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Find Type";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(0, 51);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Value";
			// 
			// comboBoxValue
			// 
			this.comboBoxValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxValue.FormattingEnabled = true;
			this.comboBoxValue.Location = new System.Drawing.Point(0, 67);
			this.comboBoxValue.Name = "comboBoxValue";
			this.comboBoxValue.Size = new System.Drawing.Size(599, 21);
			this.comboBoxValue.TabIndex = 7;
			// 
			// buttonSearch
			// 
			this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSearch.Location = new System.Drawing.Point(524, 94);
			this.buttonSearch.Name = "buttonSearch";
			this.buttonSearch.Size = new System.Drawing.Size(75, 23);
			this.buttonSearch.TabIndex = 8;
			this.buttonSearch.Text = "&Search";
			this.buttonSearch.UseVisualStyleBackColor = true;
			this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
			// 
			// certListPart
			// 
			this.certListPart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.certListPart.Location = new System.Drawing.Point(0, 123);
			this.certListPart.Name = "certListPart";
			this.certListPart.Size = new System.Drawing.Size(599, 70);
			this.certListPart.TabIndex = 9;
			// 
			// StoreView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.certListPart);
			this.Controls.Add(this.buttonSearch);
			this.Controls.Add(this.comboBoxValue);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.comboBoxFindType);
			this.Controls.Add(this.comboBoxStoreName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.comboBoxStoreLocation);
			this.Controls.Add(this.label1);
			this.Name = "StoreView";
			this.Size = new System.Drawing.Size(599, 196);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxStoreLocation;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxStoreName;
		private System.Windows.Forms.ComboBox comboBoxFindType;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboBoxValue;
		private System.Windows.Forms.Button buttonSearch;
		private CertListPart certListPart;
	}
}
