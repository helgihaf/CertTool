﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertTool
{
	internal static class ItemCertificateFactory
	{
		public static IList<ViewModel.ItemCertificate> Load(string filePath, string password)
		{
			string extension = Path.GetExtension(filePath);
			IList<X509Certificate2> certificates;
			if (IsPkcs7Extension(extension))
			{
				certificates = LoadPkcs7(filePath);
			}
			else
			{
				certificates = LoadCert(filePath, password);
			}

			return certificates.Select(
				cert => new ViewModel.ItemCertificate
				{
					Certificate = cert,
					Location = new ViewModel.FileLocation { FullPath = filePath }
				}
			).ToList();
		}

		public static bool IsPasswordProtectedFile(string filePath)
		{
			return IsPasswordProtectedExtension(Path.GetExtension(filePath));
		}

		private static bool IsPasswordProtectedExtension(string extension)
		{
			return ExtensionEquals(".pfx", extension) || ExtensionEquals(".p12", extension);
		}

		private static bool IsPkcs7Extension(string extension)
		{
			return ExtensionEquals(".spc", extension) || ExtensionEquals(".p7b", extension);
		}

		private static bool ExtensionEquals(string extension1, string extension2)
		{
			return string.Equals(extension1, extension2, StringComparison.OrdinalIgnoreCase);
		}

		private static IList<X509Certificate2> LoadPkcs7(string filePath)
		{
			IList<X509Certificate2> list = LoadBase64Pkcs7File(filePath);
			if (list == null)
			{
				list = LoadBinaryPkcs7File(filePath);
			}
			return list;
		}

		private static IList<X509Certificate2> LoadBase64Pkcs7File(string filePath)
		{
			const string BeginMarker = "-----BEGIN CERTIFICATE-----";
			const string EndMarker = "-----END CERTIFICATE-----";

			IList<X509Certificate2> list = null;
			var bytes = Base64FileParser.Parse(filePath, BeginMarker, EndMarker);
			if (bytes != null)
			{
				list = DecodePkcs7Bytes(bytes);
			}
			return list;
		}

		private static IList<X509Certificate2> LoadBinaryPkcs7File(string filePath)
		{
			var bytes = File.ReadAllBytes(filePath);
			return DecodePkcs7Bytes(bytes);
		}

		private static IList<X509Certificate2> DecodePkcs7Bytes(byte[] signature)
		{
			var cms = new SignedCms();
			cms.Decode(signature);

			if (cms.Detached)
				throw new InvalidOperationException("Cannot extract enveloped content from a detached signature.");

			var list = new List<X509Certificate2>();
			foreach (X509Certificate2 cert in cms.Certificates)
			{
				list.Add(cert);
			}

			return list;
		}

		private static IList<X509Certificate2> LoadCert(string filePath, string password)
		{
			X509Certificate2 certificate;

			if (string.IsNullOrEmpty(password))
			{
				certificate = new X509Certificate2(filePath);
			}
			else
			{
				certificate = new X509Certificate2(filePath, password);
			}
			return new List<X509Certificate2> { certificate };
		}

	}
}
