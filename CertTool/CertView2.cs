﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

namespace CertTool
{
	public partial class CertView2 : UserControl
	{
		public CertView2()
		{
			InitializeComponent();
		}

		public void LoadData(ViewModel.ItemCertificate item)
		{
			X509Certificate2 certificate = null;
			if (item != null)
			{
				certificate = item.Certificate;
				if (certificate != null)
				{
					TypeDescriptor.AddAttributes(certificate, new Attribute[] { new ReadOnlyAttribute(true) });
				}
			}
			propertyGrid.SelectedObject = certificate;
		}
	}
}
