﻿namespace CertTool
{
	partial class CertListPart
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listViewCerts = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.SuspendLayout();
			// 
			// listViewCerts
			// 
			this.listViewCerts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
			this.listViewCerts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listViewCerts.FullRowSelect = true;
			this.listViewCerts.Location = new System.Drawing.Point(0, 25);
			this.listViewCerts.Name = "listViewCerts";
			this.listViewCerts.Size = new System.Drawing.Size(473, 173);
			this.listViewCerts.TabIndex = 10;
			this.listViewCerts.UseCompatibleStateImageBehavior = false;
			this.listViewCerts.View = System.Windows.Forms.View.Details;
			this.listViewCerts.SelectedIndexChanged += new System.EventHandler(this.listViewCerts_SelectedIndexChanged);
			this.listViewCerts.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewCerts_DragDrop);
			this.listViewCerts.DragOver += new System.Windows.Forms.DragEventHandler(this.listViewCerts_DragOver);
			this.listViewCerts.DoubleClick += new System.EventHandler(this.listViewCerts_DoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Subject";
			this.columnHeader1.Width = 400;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Issuer";
			this.columnHeader2.Width = 400;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Serial";
			this.columnHeader3.Width = 160;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(473, 25);
			this.toolStrip1.TabIndex = 11;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// CertListPart
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.listViewCerts);
			this.Controls.Add(this.toolStrip1);
			this.Name = "CertListPart";
			this.Size = new System.Drawing.Size(473, 198);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView listViewCerts;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ToolStrip toolStrip1;
	}
}
