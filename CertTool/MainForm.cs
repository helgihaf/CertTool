﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertTool
{
	public partial class MainForm : Form
	{
		private StoreViewDialog storeViewDialog;
		private ViewModel.ItemCertificate currentlyShownItem;

		public MainForm()
		{
			InitializeComponent();
			storeViewDialog = new StoreViewDialog();
		}

		internal Properties.Settings Settings { get; set; }

		private void openCertificateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openFileDialog.ShowDialog(this) == DialogResult.OK)
			{
				OpenFile(openFileDialog.FileName);
			}
		}

		private bool OpenFile(string filePath)
		{
			string password = null;

			if (ItemCertificateFactory.IsPasswordProtectedFile(filePath))
			{
				if (!GetPasswordForFile(filePath, out password))
				{
					return false;
				}
			}

			IList<ViewModel.ItemCertificate> items = null;
			try
			{
				items = ItemCertificateFactory.Load(filePath, password);
			}
			catch (Exception ex)
			{
				if (ex is System.Security.Cryptography.CryptographicException ||
					ex is System.IO.IOException)
				{
					string message = $"Error opening file {filePath}\n\n{ex.GetType().FullName}: {ex.Message}";
					MessageBox.Show(this, message, "Open File", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}

			if (items != null)
			{
				OpenItemCertificates(items);
			}

			return items != null;
		}

		private bool GetPasswordForFile(string filePath, out string password)
		{
			password = null;
			bool isOk = false;
			using (var passwordDialog = new PasswordDialog())
			{
				passwordDialog.FilePath = filePath;
				if (passwordDialog.ShowDialog(this) == DialogResult.OK)
				{
					password = passwordDialog.Password;
					isOk = true;
				}
			}
			return isOk;
		}

		private void OpenItemCertificates(IList<ViewModel.ItemCertificate> items)
		{
			if (items.Count == 0)
			{
				return;
			}

			certListPart.AddData(items);
			if (certListPart.CertificateCount == items.Count)
			{
				certListPart.SelectedItem = items[items.Count - 1];
			}
		}

		private void openFromStoreToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ApplySearchSettings(storeViewDialog);
			if (storeViewDialog.ShowDialog(this) == DialogResult.OK)
			{
				OpenItemCertificates(storeViewDialog.SelectedItems);
			}
			SaveSearchSettings(storeViewDialog);
		}

		private void ApplySearchSettings(StoreViewDialog dialog)
		{
			if (Settings == null)
			{
				return;
			}

			if (Settings.LastSearch != null)
			{
				dialog.StoreSearch = Settings.LastSearch;
			}

			if (Settings.FindValueHistory != null)
			{
				dialog.FindValueHistory = new List<string>(Settings.FindValueHistory.Cast<string>());
			}
		}

		private void SaveSearchSettings(StoreViewDialog dialog)
		{
			if (Settings != null)
			{
				Settings.LastSearch = storeViewDialog.StoreSearch;
				Settings.FindValueHistory = new System.Collections.Specialized.StringCollection();
				Settings.FindValueHistory.AddRange(storeViewDialog.FindValueHistory.ToArray());
				Settings.Save();
			}
		}

		private void certListPart_SelectedCertificatesChanged(object sender, EventArgs e)
		{
			SelectItemCertificate(certListPart.SelectedItem);
		}

		private void SelectItemCertificate(ViewModel.ItemCertificate selectedItem)
		{
			if (object.ReferenceEquals(selectedItem, currentlyShownItem))
			{
				return;
			}

			certView.LoadData(selectedItem);
			currentlyShownItem = selectedItem;
		}

		private void certListPart_FileOpenRequested(object sender, FileOpenRequestEventArgs e)
		{
			foreach (var filePath in e.FilePaths)
			{
				if (!OpenFile(filePath))
				{
					break;
				}
			}
		}
	}
}
