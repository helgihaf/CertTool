﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertTool
{
	internal static class Base64FileParser
	{
		public static byte[] Parse(string filePath, string beginMarker, string endMarker)
		{
			string[] lines = File.ReadAllLines(filePath);
			int beginIndex = IndexOfLine(lines, beginMarker);
			if (beginIndex < 0)
			{
				return null;
			}

			var sb = new StringBuilder();
			for (int index = beginIndex + 1; index < lines.Length && !LineEquals(lines[index], endMarker); index++)
			{
				sb.Append(lines[index].Trim());
			}
			return Convert.FromBase64String(sb.ToString());
		}

		private static int IndexOfLine(string[] lines, string targetLine)
		{
			for (int i = 0; i < lines.Length; i++)
			{
				if (LineEquals(lines[i], targetLine))
				{
					return i;
				}
			}
			return -1;
		}

		private static bool LineEquals(string line1, string line2)
		{
			return string.Equals(line1, line2, StringComparison.Ordinal);
		}
	}
}
