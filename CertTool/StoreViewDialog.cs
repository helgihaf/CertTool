﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertTool
{
	public partial class StoreViewDialog : Form
	{
		public StoreViewDialog()
		{
			InitializeComponent();
			UpdateActions();
		}

		public StoreSearch StoreSearch
		{
			get { return storeView.StoreSearch; }
			set { storeView.StoreSearch = value; }
		}

		public IList<string> FindValueHistory
		{
			get { return storeView.FindValueHistory; }
			set { storeView.FindValueHistory = value; }
		}

		public IList<ViewModel.ItemCertificate> SelectedItems
		{
			get { return storeView.SelectedItems; }
		}

		private void storeView_SelectedCertificatesChanged(object sender, EventArgs e)
		{
			UpdateActions();
		}

		private void UpdateActions()
		{
			buttonOk.Enabled = SelectedItems.Count > 0;
		}

		private void storeView_SelectionActivated(object sender, EventArgs e)
		{
			if (SelectedItems.Count > 0)
			{
				DialogResult = DialogResult.OK;
			}
		}
	}
}
