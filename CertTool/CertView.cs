﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

namespace CertTool
{
	public partial class CertView : UserControl
	{
		public CertView()
		{
			InitializeComponent();
		}

		public void LoadData(X509Certificate2 cert)
		{
			textBoxSubject.Text = cert.Subject;
			textBoxIssuer.Text = cert.Issuer;
		}
	}
}
