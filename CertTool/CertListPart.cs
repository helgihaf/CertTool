﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;

namespace CertTool
{
	public partial class CertListPart : UserControl
	{
		private IList<ViewModel.ItemCertificate> selectedItems;

		public CertListPart()
		{
			InitializeComponent();
		}

		public IList<ViewModel.ItemCertificate> SelectedItems
		{
			get
			{
				if (selectedItems == null)
				{
					selectedItems = GetSelectedItems();
				}
				return selectedItems;
			}
		}

		public ViewModel.ItemCertificate SelectedItem
		{
			get
			{
				var items = SelectedItems;
				if (items.Count > 0)
				{
					return items[0];
				}
				return null;
			}
			set
			{
				if (value != null)
				{
					foreach (ListViewItem item in listViewCerts.Items)
					{
						if (object.ReferenceEquals(item.Tag, value))
						{
							item.Selected = true;
							break;
						}
					}
				}
				else
				{
					listViewCerts.SelectedItems.Clear();
				}
			}
		}

		public bool MultiSelect
		{
			get { return listViewCerts.MultiSelect; }
			set { listViewCerts.MultiSelect = value; }
		}

		public int CertificateCount
		{
			get { return listViewCerts.Items.Count; }
		}

		public bool AllowFileDrop
		{
			get { return listViewCerts.AllowDrop; }
			set { listViewCerts.AllowDrop = value; }
		}

		public void ClearData()
		{
			listViewCerts.Items.Clear();
		}

		public void AddData(IEnumerable<ViewModel.ItemCertificate> items)
		{
			foreach (var cert in items)
			{
				AddData(cert);
			}
		}


		public void AddData(ViewModel.ItemCertificate item)
		{
			var listViewItem = new ListViewItem();
			SetListViewItem(listViewItem, item);
			listViewCerts.Items.Add(listViewItem);
		}

		public event EventHandler<EventArgs> SelectedCertificatesChanged;
		public event EventHandler<EventArgs> SelectionActivated;
		public event EventHandler<FileOpenRequestEventArgs> FileOpenRequested;

		private void SetListViewItem(ListViewItem listViewItem, ViewModel.ItemCertificate item)
		{
			listViewItem.Text = item.Certificate.Subject;
			listViewItem.SubItems.Add(item.Certificate.Issuer);
			listViewItem.SubItems.Add(item.Certificate.SerialNumber);
			listViewItem.Tag = item;
		}

		private void listViewCerts_SelectedIndexChanged(object sender, EventArgs e)
		{
			selectedItems = null;
			OnSelectedCertificatesChanged();
		}

		private void OnSelectedCertificatesChanged()
		{
			SelectedCertificatesChanged?.Invoke(this, EventArgs.Empty);
		}

		private IList<ViewModel.ItemCertificate> GetSelectedItems()
		{
			var list = new List<ViewModel.ItemCertificate>();
			foreach (ListViewItem listViewItem in listViewCerts.SelectedItems)
			{
				list.Add((ViewModel.ItemCertificate)listViewItem.Tag);
			}
			return list;
		}

		private void listViewCerts_DoubleClick(object sender, EventArgs e)
		{
			if (SelectedItems.Count >= 0)
			{
				OnSelectionActivated();
			}
		}

		private void OnSelectionActivated()
		{
			SelectionActivated?.Invoke(this, EventArgs.Empty);
		}

		private void listViewCerts_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				e.Effect = DragDropEffects.Copy;
			}
		}

		private void listViewCerts_DragDrop(object sender, DragEventArgs e)
		{
			var fileList = new List<string>((string[])e.Data.GetData(DataFormats.FileDrop));
			if (fileList.Count > 0)
			{
				OnFileOpenRequested(fileList);
			}
		}

		private void OnFileOpenRequested(List<string> fileList)
		{
			if (FileOpenRequested != null)
			{
				var eventArgs = new FileOpenRequestEventArgs
				{
					FilePaths = fileList
				};
				FileOpenRequested(this, eventArgs);
			}
		}
	}

	public class FileOpenRequestEventArgs : EventArgs
	{
		public IList<string> FilePaths { get; set; }
	}
}
